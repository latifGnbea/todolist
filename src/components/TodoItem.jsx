import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPenToSquare } from "@fortawesome/free-solid-svg-icons";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

const TodoItem = ({ todo, toggleCompleted, deleteTodo,edit }) => {
  return (
    <div className="m-3 py-2 px-3 bg-warning rounded-3  text-start d-flex justify-content-between align-items-center fs-4">
      <p
        onClick={() => toggleCompleted(todo.id)}
        className={`${
          todo.completed
            ? "mb-0 bg-warning user-select-none cursor-pointer  text-decoration-line-through "
            : "mb-0 bg-warning user-select-none cursor-pointer "
        }`}
      >
        {todo.task}
      </p>
      <div className="bg-warning ">
        <FontAwesomeIcon
          className="bg-warning me-3  cursor-pointer"
          icon={faPenToSquare}
          onClick={() => edit(todo.id)}
        />
        <FontAwesomeIcon
          className="bg-warning cursor-pointer"
          icon={faTrash}
          onClick={() => deleteTodo(todo.id)}
        />
      </div>
    </div>
  );
};

export default TodoItem;
