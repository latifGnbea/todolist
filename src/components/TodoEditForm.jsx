import React, { useState } from "react";

const TodoEditForm = ({ editTodo, todo }) => {
  const [value, setValue] = useState(todo.task);

  const handleSubmit = (e) => {
    e.preventDefault();
    editTodo(value, todo.id);
    setValue("");
  };

  return (
    <form id="form-todo" style={{ width: "100%" }} onSubmit={handleSubmit}>
      <div className="input-group input-group-lg my-2">
        <input
          type="text"
          className="form-control outline-0 shadow-none placeholder-custom"
          placeholder="Ajouter une tache"
          aria-label="Ajouter une tache"
          aria-describedby="button-tdod"
          onChange={(e) => setValue(e.target.value)}
          value={value}
        />
        <button className="btn btn-info text-white ms-1" type="submit" id="button-tdod">
          Modifier
        </button>
      </div>
    </form>
  );
};

export default TodoEditForm;
