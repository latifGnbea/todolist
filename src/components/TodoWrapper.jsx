import React, { useState } from "react";
import TodoForm from "./TodoForm";
import { v4 as uuidv4 } from "uuid";
import TodoItem from "./TodoItem";
import TodoEditForm from "./TodoEditForm";

const TodoWrapper = () => {
  const [todos, setTodos] = useState([]);
  const addTodo = (todo) => {
    setTodos([
      ...todos,
      { id: uuidv4(), task: todo, completed: false, isEditing: false },
    ]);
  };

  const toggleCompleted = (id) => {
    setTodos(
      todos.map((todo) =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      )
    );
  };

  const deleteTodo = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id));
  };

  const edit = (id) => {
    setTodos(
      todos.map((todo) =>
        todo.id === id ? { ...todo, isEditing: !todo.isEditing } : todo
      )
    );
  };

  const editTodo = (task, id) => {
    setTodos(
      todos.map((todo) =>
        todo.id === id ? { ...todo, task, isEditing: !todo.isEditing } : todo
      )
    );
  };

  return (
    <div
      className="mt-5 border p-2 rounded-3 text-center w-75 mx-auto"
      id="todoWrapper"
    >
      <div>
        <TodoForm addTodo={addTodo} />
      </div>
      <div>
        {todos.map((todo, index) =>
          todo.isEditing ? (
            <TodoEditForm key={index} todo={todo} editTodo={editTodo} />
          ) : (
            <TodoItem
              todo={todo}
              key={index}
              toggleCompleted={toggleCompleted}
              deleteTodo={deleteTodo}
              edit={edit}
            />
          )
        )}
      </div>
    </div>
  );
};

export default TodoWrapper;
