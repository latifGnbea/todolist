import React, { useState } from "react";

const TodoForm = ({ addTodo }) => {
  const [value, setValue] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    addTodo(value);
    setValue('')
  };
  return (
    <form id="form-todo" style={{ width: "100%" }} onSubmit={handleSubmit}>
      <div className="input-group input-group-lg">
        <input
          type="text"
          className="form-control outline-0 shadow-none placeholder-custom"
          placeholder="Ajouter une tache"
          aria-label="Ajouter une tache"
          aria-describedby="button-tdod"
          onChange={(e) => setValue(e.target.value)}
          value={value}
        />
        <button className="btn btn-warning text-white ms-1" type="submit" id="button-tdod">
          Ajouter
        </button>
      </div>
    </form>
  );
};

export default TodoForm;
