// App.js
import React from "react";
import "./fontawesome";
import "./App.css";
import TodoWrapper from "./components/TodoWrapper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPenToSquare } from "@fortawesome/free-solid-svg-icons";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

const App = () => {
  return (
    <div className="mx-auto w-75 py-2 my-1 row border">
      <h1 className="mb-4 text-center">Todo list By latif</h1>
      <div className="col-md-5">
        <div className="text-center">
          <h4>
            <u>Bienvenue dans notre TodoList interactive !</u>
          </h4>
          <p className=" mx-auto">
            <i>
              Nous sommes ravis de vous accueillir sur notre plateforme dédiée à
              la gestion des tâches. La TodoList est un outil simple et efficace
              pour vous aider à organiser votre quotidien, à rester productif et
              à atteindre vos objectifs.
            </i>
          </p>
        </div>

        <div className="text-center">
          <h5>
            <u>Comment commencer ?</u>
          </h5>
          <p className="mx-auto">
            <ul className="text-start">
              <li className="mb-2">
                Ajoutez vos Tâches : Cliquez sur le bouton "Ajouter" et
                commencez à énumérer ce que vous avez à faire.
              </li>
              <li className="mb-2">
                Marquez vos Réalisations : Cliquez sur une tâche terminée pour
                la marquer comme accomplie.
              </li>
              <li className="mb-2">
                Modifier vos taches : Cliquez sur le bouton &nbsp;
                 <FontAwesomeIcon icon={faPenToSquare} /> Pour Modifier la tache
              </li>
              <li>
                Supprimer vos taches : Cliquez sur le bouton  &nbsp;
                 <FontAwesomeIcon icon={faTrash} /> Pour Supprimer la tache
              </li>
            </ul>
          </p>
        </div>
      </div>

      <div className=" col-md-7 d-flex flex-column justify-content-center">
        <TodoWrapper />
      </div>
    </div>
  );
};

export default App;
